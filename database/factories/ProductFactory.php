<?php

use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {
    return [
      'price' => $faker->randomFloat(2,1,100),
      'rating'  => $faker->randomFloat(1, 1, 5),
      'title' => $faker->sentence(6),
      'tag' => $faker->word(),
      'img'  => $faker->imageUrl(200, 200),
      'description'  => $faker->text(200),
    ];
});
