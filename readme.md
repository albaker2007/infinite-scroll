## About This project

This project have these parts

- Route
- Controller
- Model 
- DB
- DB Migration
- Seeder
- Model Factory
- Vue Components

I have used Laravel framework with vue and bootstrap. The route is in app/routers/web.php
because the project is simple I keept the controller function in the router. 
<br>
<br>


## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
