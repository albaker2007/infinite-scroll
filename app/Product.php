<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
      'price', 'rating', 'title', 'tag', 'img', 'description',
    ];
}